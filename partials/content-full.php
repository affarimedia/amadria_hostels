<section class="content">

    <div class="inner full">

        <div class="content-wrapper">
            <div class="content">
				<?php echo get_sub_field( 'content_full' ); ?>
            </div>
        </div>

    </div>

</section>