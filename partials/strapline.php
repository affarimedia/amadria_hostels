<section class="affaricontent" id="strapline">

<div class="container-fluid bg-dark">
  <div class="container">
    <div class="row py-5">
      <div class="col-md-8">
        <h3 class="content-title text-white"><?php echo the_sub_field( 'strap_title' ); ?><br class="d-none d-md-block"><?php echo the_sub_field( 'strap_subtitle' ); ?></h3>
      </div>
</div>
</div>
</div>

</section>
