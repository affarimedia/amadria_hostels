<main>
<section class="hero-image">

	<div class="inner">


		<div class="hero-image-wrapper">

			<?php
			$desktopImage = get_field('desktop_hero_image');
			$mobileImage = get_field('mobile_hero_image');
			$arrow = get_field('arrow_banner');
			$textShadow = get_field('use_text_shadow') ? 'shadow' : '';
			$content = get_field('content');
            ?>

			<div class="hero-image" style="background-image: url('<?php echo isset($mobileImage['url']) ? $mobileImage['url'] : $desktopImage['url'] ?>'); background-position: 49% top;">

				<?php
				$imgRetina = $desktopImage['url'];
				$imgNonRetina = $desktopImage['sizes']['hero-image-x1'];
				$imgAlt = isset($desktopImage['alt']) ? $desktopImage['alt'] : '';
				?>

                <img src="<?php echo $imgNonRetina ?>"
                     alt=""
                     srcset="<?php echo $imgRetina; ?> 1280w, <?php echo $imgNonRetina; ?> 960w">

                <div class="offset-arrow <?php echo $arrow ?>"></div>

			</div>

            <div class="hero-text <?php echo $textShadow; ?>">

	            <?php echo $content; ?>

            </div>

		</div>

	</div>

</section>
<div id="main-content"></div>
