<section class="affaricontent" id="intro">

  <div class="container-fluid bg-white">
    <div class="container">
      <div class="row py-5">
        <div class="col-md-8">
          <h2 class="section-title mb-4"><?php echo get_sub_field( 'intro_title' ); ?></h2>
          <p class="content-subtitle"><?php echo get_sub_field( 'intro_content' ); ?>
        </div>
  </div>
  </div>
  </div>

</section>
