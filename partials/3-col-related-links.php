<section class="affaricontent">

    <div class="inner">

        <div class="content-wrapper">


                <div class="related-links">

                    <div class="big-title">
                        <h2><?php echo get_sub_field( 'big_title_related_links' ); ?></h2>

                    </div>

                    <div class="columns">
						<?php
						$leftHeading  = get_sub_field( 'left_column_heading_related_links' );
						$leftContent  = get_sub_field( 'column_left_content_related_links' );
						$leftLinkText = get_sub_field( 'column_left_link_text_related_links' );
						$leftLink     = get_sub_field( 'column_left_link_url_related_links' );

						?>
                        <h3><?php echo $leftHeading ?></h3>

                        <p><?php echo $leftContent ?></p>

                        <a href="<?php echo $leftLink ?>">
                            <p><?php echo $leftLinkText ?></p>
                        </a>
                    </div>

                    <div class="columns">
						<?php
						$middleHeading  = get_sub_field( 'middle_column_heading_related_links' );
						$middleContent  = get_sub_field( 'column_middle_content_related_links_' );
						$middleLinkText = get_sub_field( 'column_middle_link_text_related_links' );
						$middleLink     = get_sub_field( 'column_middle_link_url_related_links' );
						?>
                        <h3><?php echo $middleHeading ?></h3>

                        <p><?php echo $middleContent ?></p>

                        <a href="<?php echo $middleLink ?>">
                            <p><?php echo $middleLinkText ?></p>
                        </a>

                    </div>

                    <div class="columns">
						<?php
						$rightHeading  = get_sub_field( 'right_column_heading_related_links' );
						$rightContent  = get_sub_field( 'column_right_content_related_links' );
						$rightLinkText = get_sub_field( 'column_right_link_text_related_links' );
						$rightLink     = get_sub_field( 'column_right_link_url_related_links' );
						?>
                        <h3><?php echo $rightHeading ?></h3>

                        <p><?php echo $rightContent ?></p>

                        <a href="<?php echo $rightContent ?>">
                            <p><?php echo $rightLinkText ?></p>
                        </a>

                    </div>

                </div>
                <div class="show_more">
		            <?php echo get_sub_field( 'show_more_related_links' ); ?>
                </div>

            </div>

    </div>

</section>
