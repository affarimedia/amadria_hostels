<section class="hero-image">

  <?php $hero_image_gallery_images = get_field( 'hero_image_gallery' ); ?>
  <?php if ( $hero_image_gallery_images ) :  ?>
  	<?php foreach ( $hero_image_gallery_images as $hero_image_gallery_image ): ?>
  		<a href="<?php echo $hero_image_gallery_image['url']; ?>">
  			<img src="<?php echo $hero_image_gallery_image['sizes']['thumbnail']; ?>" alt="<?php echo $hero_image_gallery_image['alt']; ?>" />
  		</a>
  	<p><?php echo $hero_image_gallery_image['caption']; ?></p>
  	<?php endforeach; ?>
  <?php endif; ?>

</section>
