<section class="affaricontent">

    <div class="container">

        <div class="inner">
			<?php
			$title = get_sub_field( 'title_accordion' );
			$text  = get_sub_field( 'text_accordion' );
			?>
            <div class="wrap">

                        <button class="accordion"><?php echo $title; ?></button>

                        <div class="accordion-panel">
                          <p><?php echo $text; ?></p>
                        </div>

            </div>
         </div>
    </div>
</section>
