                      === Affari Theme for Wordpress ===

----------------------------------------
setup
----------------------------------------
Install Dependencies

In CLI / Terminal

Change directory to theme folder: cd wp-content/theme/THEME-NAME

on terminal type:
$ npm install

Open webpack.config.js and change the Browser Sync proxy (line 4) to the dev URL of your choice (Example: http://affari.local/)


----------------------------------------
Scripts
----------------------------------------
In CLI / Terminal

$ npm run bulid
runs build command to compile SCSS and Javascript.

$ npm run watch
runs the watch command for changes in the SCSS and Javascript files then compiles them.


----------------------------------------
!important
----------------------------------------
1) install npm global
$ npm install -g

2) install on project folder the composer
$ php composer.phar install
-- Composer is a tool for dependency management in PHP. It allows you to declare the libraries your project depends on and it will manage (install/update) them for you

----------------------------------------
Licenses & Credits
----------------------------------------
- Font Awesome: http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
- Bootstrap: http://getbootstrap.com | https://github.com/twbs/bootstrap/blob/master/LICENSE (Code licensed under MIT documentation under CC BY 3.0.)
- Owl Carousel 2: http://www.owlcarousel.owlgraphic.com/ | https://github.com/smashingboxes/OwlCarousel2/blob/develop/LICENSE (Code licensed under MIT)
and of course
- jQuery: https://jquery.org | (Code licensed under MIT)

----------------------------------------
custom admin style
----------------------------------------
on login folder, insert your logo as 'logo.png' and your background image as 'Surface.webp'
