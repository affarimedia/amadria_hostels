<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php wp_title(''); ?></title>

    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
    <!--[if IE]>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <![endif]-->
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>

    <?php // drop Google Analytics Here ?>

    <?php // end analytics ?>

</head>

<body <?php body_class('no-js'); ?> itemscope itemtype="http://schema.org/WebPage">

<section class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
    <div class="header-container">
        <div class="header-wrap">
            <div class="logo">
                <a href="<?php echo site_url(); ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/images/logo.png" alt="Logo">
                </a>
            </div>
            <a href="#" id="mobile-navigation-toggle" class="navigation-toggle" role="button" aria-label="navigation toggle" aria-controls="navigation-primary" aria-expanded="false">
                <div class="nav-toggle">
                    <span class="icon-bar">
                    <span></span>
                    </span>
                </div>
            </a>
            <div class="second-part">
                <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                    <?php
                    wp_nav_menu(array(
                        'menu' => 'Main Menu'
                    ));
                    ?>
                </nav>

            </div>
        </div>
    </div>
</section>

<div class="clear"></div>
<div class="main">
