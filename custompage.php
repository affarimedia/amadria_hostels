<?php /* Template Name: Display Rooms */ ?>

<?php get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

      <?php
      $posts = get_posts(array(
      	'posts_per_page'	=> -1,
        'orderby'         => rand,
        'order'           => DESC,
      	'post_type'			  => 'room'
      ));

      if( $posts ): ?>

      	<?php foreach( $posts as $post ):

      		setup_postdata( $post );

      		?>
      		<p><i class="fas fa-hotel"></i></p>
          <p>
            <?php if ( have_rows( 'rooms_posts_af' ) ): ?>
            	<?php while ( have_rows( 'rooms_posts_af' ) ) : the_row(); ?>
            		<?php if ( get_row_layout() == 'rooms' ) : ?>
            			<?php the_sub_field( 'rooms_title' ); ?>
            			<?php the_sub_field( 'rooms_descriptions' ); ?>
            			<?php the_sub_field( 'rooms_bed_descriptions' ); ?>
            			<?php the_sub_field( 'rooms_location' ); ?>
            			<?php the_sub_field( 'room_details_amenities' ); ?>
            			<?php $more_photos_images_images = get_sub_field( 'more_photos_images' ); ?>
            			<?php if ( $more_photos_images_images ) :  ?>
            				<?php foreach ( $more_photos_images_images as $more_photos_images_image ): ?>
            					<a href="<?php echo $more_photos_images_image['url']; ?>">
            						<img src="<?php echo $more_photos_images_image['sizes']['thumbnail']; ?>" alt="<?php echo $more_photos_images_image['alt']; ?>" />
            					</a>
            				<p><?php echo $more_photos_images_image['caption']; ?></p>
            				<?php endforeach; ?>
            			<?php endif; ?>
            		<?php endif; ?>
            	<?php endwhile; ?>
            <?php else: ?>
            	<?php // no layouts found ?>
            <?php endif; ?>
      		</p>

      	<?php endforeach; ?>

      	<?php wp_reset_postdata(); ?>

      <?php endif; ?>

    </main>

</div>
<div style = "clear:both"></div>
<?php get_footer(); ?>
