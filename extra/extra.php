<?php
/**
 * functions.php extras
 * This file is used for all utility functions.
 * Please place all add_action calls, with their relevant functions within actions.php
 */

/**
 * Loops through the flexible content field and displays any partials that have templates within /partials/
 */
function affari_display_flexible_content()
{
	global $post;

	if ( have_rows('flexible_content', $post->ID) ) {
		while ( have_rows('flexible_content', $post->ID ) ) {
			the_row();

			$fileName = __DIR__ . '/../partials/' . get_row_layout() . '.php';

			if ( file_exists($fileName) ) {
				include($fileName);
			}
		}
	}
}

/** display Romms */
function affari_display_amadria_rooms()
{
	global $post;

	if ( have_rows('rooms_posts_af', $post->ID) ) {
		while ( have_rows('rooms_posts_af', $post->ID ) ) {
			the_row();

			$fileName = __DIR__ . '/../rooms/' . get_row_layout() . '.php';

			if ( file_exists($fileName) ) {
				include($fileName);
			}
		}
	}
}

function affari_display_hero_image_room()
{
	include(__DIR__ . '/../rooms/hero-image-room.php');
}

/**
 * Includes the hero image / videos / gallery
 */
function affari_display_hero_section()
{
	include(__DIR__ . '/../partials/hero-image.php');
}

function affari_display_hero_video()
{
	include(__DIR__ . '/../partials/hero-video.php');
}

function affari_display_hero_gallery()
{
	include(__DIR__ . '/../partials/hero-image-gallery.php');
}

/** embed video full for hero video */

function affari_function_embed_video ( $html ) {

  return '<div class="heropanel--video">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'affari_function_embed_video ', 10, 3 );
add_filter( 'video_embed_html', 'affari_function_embed_video ' );

/** affari Rooms --fixed on Admin menu after posts */

function affari_post_type() {
  register_post_type( 'room',
    array(
      'labels' => array(
        'name' => __( 'Rooms' ),
        'singular_name'         => __( 'Room' ),
        'all_items'             => 'All Rooms',
        'add_new_item'          => 'Add New Room',
        'add_new'               => 'Add Room page',
        'new_item'              => 'New Room',
        'edit_item'             => 'Edit Room',
        'update_item'           => 'Update Room',
        'view_item'             => 'View Room',
        'view_items'            => 'View Rooms',
        'search_items'          => 'Search Room'
      ),
      'public'                  => true,
      'has_archive'             => true,
      'rewrite'                 => true,
      'menu_position'           => 5,
      'menu_icon'               => 'dashicons-admin-network',
    )
  );
}
add_action( 'init', 'affari_post_type' );
flush_rewrite_rules( false );

/**
 * Alternative to the_excerpt.
 *
 * Pass any string through this function along with a maximum character limit
 *
 * @param $string string - No HTML, use strip_tags() on any HTML content prior to using this function
 * @param $your_desired_width int - The desired width in characters, how strict this is maintained is controllable via the $strict argument
 * @param $strict bool - default true
 * How strict the character limit should be. If set to true, this will remove any words (even if in the middle of a word) that exceed the character limit.
 * If set to false, this will allow a word to go over the character limit, if the character limit ended in the middle of that word
 *
 * Example: "Hello World" has 11 characters (1 space, 10 letters). If the character limit was set to 9, strict would behave in the following way:
 *
 *     True: would return "Hello..."
 *     False: Would Return "Hello World" Because 9 characters ends on the 'r' of World, it will complete the word and THEN end.
 *
 * @return bool|string
 */
function affari_truncate_string( $string, $your_desired_width, $strict = true ) {
	$parts       = preg_split( '/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE );
	$parts_count = count( $parts );

	$greater = false;

	$startingLength = strlen($string);
	$length    = 0;
	$last_part = 0;

	for ( ; $last_part < $parts_count; ++ $last_part ) {
		$length += strlen( $parts[ $last_part ] );
		if ( $length > $your_desired_width ) {
			$greater = true;
			if ( !$strict ) {
				$last_part++;
			}
			break;
		}
	}

  if ( $startingLength === $length ) {
		$greater = false;
	}

	$finalString = implode( array_slice( $parts, 0, $last_part ) );
	$finalChar   = substr( $finalString, - 1 );

	while ( $finalChar === ',' || $finalChar === ' ' || $finalChar === '.' ) {
		$finalString = substr( $finalString, 0, - 1 );
		$finalChar   = substr( $finalString, - 1 );
	}

	return $greater ? $finalString . '...' : $finalString;
}
