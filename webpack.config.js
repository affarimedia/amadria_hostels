/* Project config */

const useBrowserSync = true;
const devURL = 'http://amadria.hostels:8888';

/* End Project config */

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

let pluginArray = [
    new ExtractTextPlugin({
        filename: 'dist/style.css',
        allChunks: true
    }),
    new HardSourceWebpackPlugin(),
    new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery"
    })
];

if (useBrowserSync) {
    const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
    pluginArray.push(
        new BrowserSyncPlugin({
                host: 'localhost',
                port: 3000,
                proxy: devURL,
                files: ["dist/*.css", "dist/*.js"]
            },
            {
                injectCss: true
            }
        )
    );
}

module.exports = {
    entry: [
        './src/js/app.js',
        './src/scss/style.scss'
    ],
    output: {
        filename: 'dist/bundle.js'
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.(css|scss)$/,
                exclude: /node_modules/,
                include: path.resolve(__dirname, "src"),
                use: ExtractTextPlugin.extract({
                    use: [
                        {loader: 'postcss-loader', options: {sourceMap: true}},
                        {loader: 'sass-loader', options: {sourceMap: true}}
                    ]
                })
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }

        ]
    },
    plugins: pluginArray
};
