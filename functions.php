<?php
require_once( 'extra/affari.php' );
require_once( 'extra/extra.php' );
require_once( 'extra/actions.php' );

function affari_init() {
	// let's get language support going, if you need it
	load_theme_textdomain( 'affari', get_template_directory() . '/library/translation' );

	// launching operation cleanup
	add_action( 'init', 'affari_head_cleanup' );
	// A better title
	add_filter( 'wp_title', 'rw_title', 10, 3 );
	// remove WP version from RSS
	add_filter( 'the_generator', 'affari_rss_version' );
	// remove pesky injected css for recent comments widget
	add_filter( 'wp_head', 'affari_remove_wp_widget_recent_comments_style', 1 );
	// clean up comment styles in the head
	add_action( 'wp_head', 'affari_remove_recent_comments_style', 1 );
	// enqueue base scripts and styles
	add_action( 'wp_enqueue_scripts', 'affari_scripts_and_styles', 999 );
	// ie conditional wrapper

	affari_custom_image_sizes();

	affari_theme_support();
}

// let's get this party started
add_action( 'after_setup_theme', 'affari_init' );

/**
 * Custom functions
 */
register_nav_menus( array(
	'top_links' => 'Top Links',
) );

//// custom ADMIN

// start login style

function my_custom_login() {
echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-styles.css" />';
}
add_action('login_head', 'my_custom_login');

function my_login_logo_url() {
return get_bloginfo( 'affarimedia.com' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
return 'Affari Media';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function login_error_override()
{
    return 'Incorrect login details.';
}
add_filter('login_errors', 'login_error_override');

function my_login_head() {
remove_action('login_head', 'wp_shake_js', 12);
}
add_action('login_head', 'my_login_head');

// end login style

// admin style
function insert_admin_stylesheet() {
        wp_register_style( 'admin_style', get_template_directory_uri(). '/login/adminstyles.css', false, '1.0.0' );
        wp_enqueue_style( 'admin_style' );
}
add_action( 'admin_enqueue_scripts', 'insert_admin_stylesheet' );

function wpse_edit_footer() {
    add_filter( 'admin_footer_text', 'wpse_edit_text', 11 );
}

function wpse_edit_text($content) {
    return "proud to be made by @AffariMedia";
}

add_action( 'admin_init', 'wpse_edit_footer' );

function my_footer_shh() {
    remove_filter( 'update_footer', 'core_update_footer' );
}
add_action( 'admin_menu', 'my_footer_shh' );

// end admin style custom


// Hide the editor on a page with a specific page template
add_action( 'admin_head', 'hide_editor' );
function hide_editor() {
	$template_file = $template_file = basename( get_page_template() );
	if($template_file == 'custompage.php'){
		remove_post_type_support('page', 'editor');
	}
}

// disable widgets start
   add_filter( ‘sidebars_widgets’, ‘disable_all_widgets’ );
   function disable_all_widgets( $sidebars_widgets )
   {
      if ( is_home() ) $sidebars_widgets = array( false );
      return $sidebars_widgets;
   }
// disable widget end


?>
