<section class="affaricontent">

    <div class="container">

        <div class="inner">

          		<?php the_sub_field( 'rooms_title' ); ?>
          		<?php the_sub_field( 'rooms_descriptions' ); ?>
          		<?php the_sub_field( 'rooms_bed_descriptions' ); ?>
          		<?php the_sub_field( 'rooms_location' ); ?>
          		<?php the_sub_field( 'room_details_amenities' ); ?>
          		<?php $more_photos_images_images = get_sub_field( 'more_photos_images' ); ?>
          		<?php if ( $more_photos_images_images ) :  ?>
          			<?php foreach ( $more_photos_images_images as $more_photos_images_image ): ?>
          				<a href="<?php echo $more_photos_images_image['url']; ?>">
          					<img src="<?php echo $more_photos_images_image['sizes']['thumbnail']; ?>" alt="<?php echo $more_photos_images_image['alt']; ?>" />
          				</a>
          			<p><?php echo $more_photos_images_image['caption']; ?></p>
          			<?php endforeach; ?>
          		<?php endif; ?>

</div>
    </div>
</section>
