const $ = jQuery;
// Import additional libraries here

$(document).ready(function(){
    console.log('working and ready!');
});

// accordion
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
/// accordion end

/// start carousel
var $f = jQuery.noConflict(true);
  $f(window).load(function() {
    // The slider being synced must be initialized first
    $f('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 75,
    itemMargin: 5,
    asNavFor: '#slider'
    });

    $f('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
    });
  });
  // end carousel

// scrolling NAV start
// Smooth scrolling using jQuery easing
$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
  if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
      $('html, body').animate({
        scrollTop: (target.offset().top - 50)
      }, 2000, "easeInOutSine");
      return false;
    }
  }
});

// Closes responsive menu when a scroll trigger link is clicked
$('.js-scroll-trigger').click(function() {
  $('.navbar-collapse').collapse('hide');
});

// Activate scrollspy to add active class to navbar items on scroll
$('body').scrollspy({
  target: '#mainNav',
  offset: 54
});
// scrolling NAV end

// sticky NAV start
$(document).ready(function() {
// Custom
var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
var stickyHeight = sticky.outerHeight();
var stickyTop = stickyWrapper.offset().top;
if (scrollElement.scrollTop() >= stickyTop){
stickyWrapper.height(stickyHeight);
sticky.addClass("is-sticky");
}
else{
sticky.removeClass("is-sticky");
stickyWrapper.height('auto');
}
};

// Find all data-toggle="sticky-onscroll" elements
$('[data-toggle="sticky-onscroll"]').each(function() {
var sticky = $(this);
var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
sticky.before(stickyWrapper);
sticky.addClass('sticky');

// Scroll & resize events
$(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
stickyToggle(sticky, stickyWrapper, $(this));
});

// On page load
stickyToggle(sticky, stickyWrapper, $(window));
});
});
// sticky NAV end
