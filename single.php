<?php
get_header();

if ( get_field('has_hero_image_room') ) {
	affari_display_hero_image_room();
}

affari_display_amadria_rooms();

get_footer();

?>
